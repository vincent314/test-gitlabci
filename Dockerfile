FROM nginx:alpine

COPY dist /usr/share/nginx/html

RUN mkdir -p /etc/nginx/conf.d/

RUN echo 'server { \
            listen      80; \
            location / { \
                root    /usr/share/nginx/html/; \
                index   index.html index.htm; \
                expires -1; \
            } \
          }' > /etc/nginx/conf.d/default.conf